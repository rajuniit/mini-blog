$(document).ready(function(){
    // validate registration form
    $('#registerHere input').hover(function()
    {
        $(this).popover('show')
    });
    $("#registerHere").validate({
        rules:{
            full_name:"required",
            email:{
                required:true,
                email: true
            },
            password:{
                required:true,
                minlength: 6
            },
            confirm_password:{
                required:true,
                equalTo: "#password"
            },
            blog_title:"required"
        },
        messages:{
            full_name:"Enter your first and last name",
            email:{
                required:"Enter your email address",
                email:"Enter valid email address"
            },
            password:{
                required:"Enter your password",
                minlength:"Password must be minimum 6 characters"
            },
            confirm_password:{
                required:"Enter confirm password",
                equalTo:"Password and Confirm Password must match"
            },
            blog_title:"Please Enter a Blog Title"
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    //validate login form
    $('#loginHere input').hover(function()
    {
        $(this).popover('show')
    });

    $("#loginHere").validate({
        rules:{
            email:{
                required:true,
                email: true
            },
            password:{
                required:true,
                minlength: 6
            }
        },
        messages:{

            email:{
                required:"Enter your email address",
                email:"Enter valid email address"
            },
            password:{
                required:"Enter your password",
                minlength:"Password must be minimum 6 characters"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    //validate post form

    $('#posts input, textarea').hover(function()
    {
        $(this).popover('show')
    });

    $("#posts").validate({
        rules:{
            title:{
                required:true
            },
            content:{
                required:true
            },

            keywords:{
                required:true
            }
        },
        messages:{

            title:{
                required:"Enter the post title here"
            },
            content:{
                required:"Enter the post content"
            },
            keywords:{
                required:"Enter some keywords"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });


});
