<?php

/**
 * This class use to set config at registry. We can use
 * this class to set and get config
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class ApplicationConfig
{
    /**
     * constant
     */
    const KEY = 'App_Config';

    /**
     * @var string
     */
    protected $_path;

    /**
     * @param null $configPath
     */
    public function __construct($configPath = null)
    {

        if ($configPath) {
            $this->_path = $configPath;
        }
        if (!Zend_Registry::isRegistered(self::KEY)) {
            $this->setConfig();
        }
    }

    /**
     * @param null $path
     */
    public function setConfigPath($path = null)
    {
        if (!empty($path)) {
            $this->_path = $path;
        }
    }

    /**
     * @return null|string
     */
    public function getConfigPath()
    {
        return $this->_path;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return Zend_Registry::get(self::KEY);
    }

    /**
     * this function set config at registry
     * @return null
     */
    public function setConfig()
    {
        if (empty($this->_path)) {
            $this->_path = APP_ROOT_PATH . DIRECTORY_SEPARATOR . '../config' . DIRECTORY_SEPARATOR . 'application.ini';
        }

        if (file_exists($this->_path)) {
            $config = new Zend_Config_Ini($this->_path);
            Zend_Registry::set(self::KEY, $config);
        }
    }

}