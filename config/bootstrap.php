<?php
/**
 * Applicaton Bootstrap Class. Alos work like base controller
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 */

require_once 'application_config.php';

class Bootstrap
{
    /**
     * @var Blog_View
     */
    protected $_view;

    /**
     * @var mixed
     */
    protected $_config;

    /**
     * @param null $configPath
     */
    public function __construct($configPath = null)
    {
        $config = new ApplicationConfig($configPath);
        $this->_config = $config->getConfig();
    }

    /**
     * this function dispatch all the request
     */
    public function dispatch()
    {

        $this->_view = Blog_Handler::view();

        $requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        if (strpos($requestUri, "/") === 0) {
            $requestUri = substr($requestUri, 1);
        }

        $request = explode("/", $requestUri);
        $request = array_values(array_filter($request));

        //default request values

        $controllerName = 'homeController';
        $method = 'index';
        $params = array();

        if (!empty($request)) {
            $controllerName = $request[0] . 'Controller';
            if (isset($request[1]) && !empty($request[1])) {
                $method = $request[1];
            }

            $params = $request;
        }

        $objController = new $controllerName();

        if (is_callable(array($objController, $method))) {
            call_user_func_array(array($objController, $method), $params);
        } else {
            array_unshift($params, $method);
            call_user_func_array(array($objController, "index"), $params);
        }

        $this->_view->render();
        session_write_close();

    }

    /**
     * @param string $url
     */
    public function redirect($url = null)
    {
        $redirect = $this->_config->web->url;
        if ($url) {
            $redirect .= $url;
            $url = trim($url);
            if (strpos($url, "http") === 0) {
                $redirect = $url;
            }
        }
        header("Location: {$redirect}");
    }

    /**
     * @return Blog_View
     */
    public function view()
    {
        return Blog_Handler::view();
    }

    /**
     * @return PDO
     */
    public function db()
    {
        return Blog_Db::_initDbConnection(Zend_Registry::get('App_Config'));
    }

    /**
     * @return bool
     */
    public function logged_in()
    {
        if (isset($_SESSION['user_id'])) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function current_user()
    {
        if (isset($_SESSION['user_id'])) {
            return $_SESSION['user_id'];
        }

        return 0;
    }

    /**
     * @return mixed
     */
    public function checkAuthentication()
    {
        if (isset($_SESSION['user_id'])) {
            return true;
        }

        $this->redirect("/users/login");
    }

    public function setFlashMessage($message, $msgClass)
    {
        $this->view()->addViewParam("show_flash", true);
        $this->view()->addViewParam("flash_messages", $message);
        $this->view()->addViewParam("flash_class", $msgClass);
    }

}