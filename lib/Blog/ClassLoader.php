<?php
/**
 * It helps to autoload class. It uses symfony universal
 * class loader.
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
include_once 'Blog/UniversalClassLoader.php';

class Blog_ClassLoader extends Blog_UniversalClassLoader
{

    /**
     * @param $class
     * @return mixed
     */
    public function loadClass($class)
    {
        if (class_exists($class, false) || interface_exists($class, false)) {
            return;
        }
        $found = parent::loadClass($class);

        if (false === strripos($class, '\\')) {
            if (!$found) {
                $file = str_replace('_', DIRECTORY_SEPARATOR, $class) . '.php';
                require_once $file;
            }
        }
    }
}