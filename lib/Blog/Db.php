<?php

/**
 * Its a pdo db class to init connection.
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class Blog_Db
{

    /**
     * @var PDO
     */
    private static $_dbConn;


    /**
     * @static
     * @param $config
     * @return PDO
     */
    public static function _initDbConnection($config)
    {
        if (!self::$_dbConn) {

            $host = $config->db->host;
            $user = $config->db->user;
            $password = $config->db->password;
            $dbname = $config->db->dbname;

            try {
                $dsn = "mysql:host={$host};dbname={$dbname};";
                self::$_dbConn = new PDO($dsn, $user, $password);
                self::$_dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                self::$_dbConn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                die("Could not connect" . $e->getMessage());
            }
        }
        return self::$_dbConn;
    }

}