<?php

/**
 * It helps to lazy load the class.
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class Blog_Handler
{
    /**
     * @var Blog_View
     */
    private static $_view;

    /**
     * @var Blog_Db
     */
    private static $_db;

    /**
     * @static
     * @return Blog_View
     */
    public static function view()
    {
        if (!self::$_view) {
            self::$_view = new Blog_View();
        }

        return self::$_view;
    }

    /**
     * @static
     * @param $config
     * @return Blog_Db
     */
    public static function db($config)
    {
        if (!self::$_db) {
            self::$_db = new Blog_Db($config);
        }
        return self::$_db;
    }
}