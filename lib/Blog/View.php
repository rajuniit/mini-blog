<?php

/**
 * This class is responsible to render html to browser.
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class Blog_View
{
    /**
     * @var array
     */
    protected $_views = array();

    /**
     * @var array
     */
    protected $_view_params = array();

    /**
     * @var string
     */
    protected $_default_layout = 'layout';

    /**
     * This function include predifined view files and set values
     * @return string
     */
    public function render()
    {
        $this->_view_params['views'] = $this->_views;
        extract($this->_view_params);
        ob_start();
        include $this->_getLayout() . '.php';
        $templs = ob_get_clean();
        echo $templs;
    }

    /**
     * @param $view
     */
    public function addView($view)
    {
        $view = trim($view);
        if (substr($view, -4) != ".php") {
            $view .= ".php";
        }
        $this->_views[] = $view;

    }

    /**
     * @param $key
     * @param $val
     */
    public function addViewParam($key, $val)
    {
        $this->_view_params[$key] = $val;
    }

    /**
     * @return string
     */
    protected function _getLayout()
    {
        return $this->_default_layout;
    }

    /**
     * @param $layout
     */
    public function setLayout($layout)
    {
        $this->_default_layout = $layout;
    }
}