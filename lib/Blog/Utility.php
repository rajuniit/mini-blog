<?php

/**
 * It's utility class to define some commonly function
 *
 * @package Library
 * @author Raju Mazumder <rajuniit@gmail.com>
 *
 * Notes: Takes code from codeigniter framework and modified
 */
class Blog_Utility
{
    /**
     * @param $str
     * @param string $separator
     * @return string
     */
    public function slug($str, $separator = '-')
    {
        $q_separator = preg_quote($separator);

        $trans = array(
            '&.+?;' => '',
            '[^a-z0-9 _-]' => '',
            '\s+' => $separator,
            '(' . $q_separator . ')+' => $separator
        );

        $str = strip_tags(($str));
        foreach ($trans as $key => $val) {
            $str = preg_replace("#" . $key . "#i", $val, $str);
        }

        return trim(strtolower($str));
    }

    /**
     * @param $str
     * @param int $limit
     * @param string $end_char
     * @return string
     */
    public function word_limiter($str, $limit = 100, $end_char = '&#8230;')
    {
        if (trim($str) == '') {
            return $str;
        }

        preg_match('/^\s*+(?:\S++\s*+){1,' . (int)$limit . '}/', $str, $matches);

        if (strlen($str) == strlen($matches[0])) {
            $end_char = '';
        }

        return rtrim($matches[0]) . $end_char;
    }
}