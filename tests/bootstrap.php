<?php
/**
 * It will bootstrap the application to test
 *
 * @package Core
 * @author rajuniit@gmail.com<Raju Mazumder>
 */
date_default_timezone_set('Asia/Dhaka');

defined('APP_ROOT_PATH') || define('APP_ROOT_PATH', realpath(dirname(__FILE__)));
defined('APP_PATH') || define('APP_PATH', APP_ROOT_PATH . DIRECTORY_SEPARATOR . '../apps');
defined('LIB_PATH') || define('LIB_PATH', APP_ROOT_PATH . DIRECTORY_SEPARATOR . '/../lib');


set_include_path(get_include_path().
        PATH_SEPARATOR. realpath(LIB_PATH).
        PATH_SEPARATOR.realpath(APP_PATH . '/controllers').
        PATH_SEPARATOR.realpath(APP_PATH . '/views').
        PATH_SEPARATOR.realpath(APP_PATH . '/layouts').
        PATH_SEPARATOR.realpath(APP_PATH . '/models').
        PATH_SEPARATOR.realpath(APP_PATH . '/helpers')
);

include_once 'Blog/ClassLoader.php';

$classLoader = new Blog_ClassLoader();
$classLoader->register();

require_once '../config/bootstrap.php';

if (session_id() === '') {
    session_start();
}

$configPath = APP_ROOT_PATH .DIRECTORY_SEPARATOR.'/config'.DIRECTORY_SEPARATOR.'application.ini';
$application = new Bootstrap($configPath);
$application->dispatch();