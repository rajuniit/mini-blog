<?php

require_once dirname(__FILE__). '/../phpUnitHelper.php';

require_once APP_ROOT_PATH . '/../config/application_config.php';

class ApplicationConfigTest extends PhpUnitHelper
{
    protected $_applicationConfig;

    protected function setup()
    {
        $configPath = APP_ROOT_PATH .DIRECTORY_SEPARATOR.'/config'.DIRECTORY_SEPARATOR.'application.ini';
        $this->_applicationConfig = new ApplicationConfig($configPath);
    }

    public function testReadApplicationConfigSetProperly()
    {
        $config = $this->_applicationConfig->getConfig();
        $this->assertEquals('blog_test', $config->db->dbname);
    }
}