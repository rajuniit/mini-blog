<?php

require_once dirname(__FILE__). '/../../phpUnitHelper.php';


class PostModelTest extends PhpUnitHelper
{
    protected $_postModel;

    protected $_userModel;

    protected $_userId;

    protected function setup()
    {
        $this->_postModel = new PostModel();
    }

    public function testAddPostForValidData()
    {
        $this->_signInUser();
        $params = array(
            'title'         => 'Post title',
            'content'       => 'Post Content Goes Here',
            'keywords'      => 'general',
            'published'     => '1',
            'updated_at'    => date("Y-m-d"));

        $postId = $this->_postModel->add($params);
        $post = $this->_postModel->getPostByUserIdAndId($this->_userId, $postId);
        $this->assertTrue($post[0]['title'] == 'Post title');

    }

    public function testSearchBykeyword()
    {
        $this->_signInUser();
        $params = array(
            'title'         => 'Post title',
            'content'       => 'Post Content Goes Here',
            'keywords'      => 'general',
            'published'     => '1',
            'updated_at'    => date("Y-m-d"));

        $this->_postModel->add($params);
        $post = $this->_postModel->searchByKeyword('general', $this->_userId);
        $this->assertTrue($post[0]['title'] == 'Post title');

    }

    public function testUpdatePostForValidData()
    {
        $this->_signInUser();
        $params = array(
            'title'         => 'Post title',
            'content'       => 'Post Content Goes Here',
            'keywords'      => 'general',
            'published'     => '1',
            'updated_at'    => date("Y-m-d"));

        $postId = $this->_postModel->add($params);

        $params = array(
            'title'         => 'Updated Post title',
            'content'       => 'Post Content Goes Here',
            'keywords'      => 'general',
            'published'     => '1',
            'updated_at'    => date("Y-m-d"));
        $this->_postModel->update($postId, $params);

        $post = $this->_postModel->getPostByUserIdAndId($this->_userId, $postId);
        $this->assertTrue($post[0]['title'] == 'Updated Post title');

    }

    public function testPostDestroyForValidPostId()
    {
        $this->_signInUser();
        $params = array(
            'title'         => 'Post title',
            'content'       => 'Post Content Goes Here',
            'keywords'      => 'general',
            'published'     => '1',
            'updated_at'    => date("Y-m-d"));

        $postId = $this->_postModel->add($params);

        $this->_postModel->destroy($postId, $this->_userId);
        $this->assertFalse(count($this->_postModel->getById($postId)) > 0);

    }

    protected function _signInUser()
    {
        $this->_userModel = new UserModel();

        $data = array(
            'email'         => 'rajuniit@gmail.com',
            'full_name'     => 'Raju Mazumder',
            'blog_title'    => 'Blot Title',
            'password'      => '123456',
            'updated_at'    => date("Y-m-d"));


        $this->_userId = $this->_userModel->add($data);
        //signed in user
        $_SESSION['user_id'] = $this->_userId;
    }


}