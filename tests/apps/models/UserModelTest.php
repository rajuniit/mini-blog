<?php

require_once dirname(__FILE__). '/../../phpUnitHelper.php';


class UserModelTest extends PhpUnitHelper
{
    protected $_userModel;

    protected function setup()
    {
        $this->_userModel = new UserModel();
    }

    public function testAddUserForValidData()
    {
        $data = array(
            'email' => 'rajuniit@gmail.com',
            'full_name' => 'Raju Mazumder',
            'blog_title' => 'Blot Title',
            'password' => '123456',
            'updated_at' => date("Y-m-d"));


        $userId = $this->_userModel->add($data);
        $user = $this->_userModel->getById($userId);
        $this->assertEquals('rajuniit@gmail.com', $user[0]['email']);
    }

    public function testUserAuthenticateSuccessfullyForValidEmailAndPassword()
    {
        $data = array(
            'email' => 'rajuniit@gmail.com',
            'full_name' => 'Raju Mazumder',
            'blog_title' => 'Blot Title',
            'password' => '123456',
            'updated_at' => date("Y-m-d"));
        $this->_userModel->add($data);

        $params = array('email' => 'rajuniit@gmail.com', 'password' => '123456');
        $data = $this->_userModel->authenticate($params);
        $this->assertEquals('rajuniit@gmail.com', $data[0]['email']);

        $params = array('email' => 'rajuniit@gmail.com', 'password' => '');
        $this->assertFalse($this->_userModel->authenticate($params));
    }

    public function testUserCreationFailedForDuplicationEmail()
    {
        $data = array(
            'email' => 'rajuniit@gmail.com',
            'full_name' => 'Raju Mazumder',
            'blog_title' => 'Blot Title',
            'password' => '123456',
            'updated_at' => date("Y-m-d"));
        $this->_userModel->add($data);

        $data = array(
            'email' => 'rajuniit@gmail.com',
            'full_name' => 'Raju Mazumder',
            'blog_title' => 'Blot Title',
            'password' => '123456',
            'updated_at' => date("Y-m-d"));

        $this->assertFalse($this->_userModel->add($data));
    }

}