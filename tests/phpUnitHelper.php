<?php
require_once 'bootstrap.php';

class PhpUnitHelper extends PHPUnit_Framework_TestCase
{

    protected function tearDown()
    {
        $configPath = APP_ROOT_PATH .DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'application.ini';
        $bootstrap = new Bootstrap($configPath);
        //users table
        $sql = "Delete from users";
        $std = $bootstrap->db()->prepare($sql);
        $std->execute();

        //posts table
        $sql = "Delete from posts";
        $std = $bootstrap->db()->prepare($sql);
        $std->execute();

        //keywords table
        $sql = "Delete from keywords";
        $std = $bootstrap->db()->prepare($sql);
        $std->execute();

        //post_keywords
        $sql = "Delete from post_keywords";
        $std = $bootstrap->db()->prepare($sql);
        $std->execute();
    }

}