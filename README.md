# Instructions

1. Just unzip the source code and paste to your web folder.
2. Change the database settings at config/application.ini
3. Create the a database as per settings you give at config/application.ini
4. Import the database schema from schema/database.dump.sql
5. You will need Apache Rewrite Module on because I use .htaccess file.
6. And open your browser and run it.

# Instructions to run test.

1. Install phpunit.
2. Change the settings at tests/config/application.ini
3. Open command prompt and run the test to go to tests/ folder

