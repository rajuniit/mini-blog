<?php

/**
 * this model handles the post create,upate,delete
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class PostModel extends BaseModel
{
    /**
     * @var array
     */
    protected $_required_fields = array('title' => 'title',
        'content' => 'content', 'keywords' => 'keywords');

    /**
     * init security and validator class
     */
    public function __construct() {
        $this->_dbTable = "posts";
        $this->_validator = new ValidatorModel();
        $this->_security  = new Blog_Security();
    }

    /**
     * @param $keyword
     * @param $userId
     * @return array
     */
    public function searchByKeyword($keyword, $userId)
    {
        $keyword = $this->_security->xss_clean($keyword);
        $sql = "SELECT p.* FROM posts p LEFT JOIN post_keywords pk ON
                p.id = pk.post_id LEFT JOIN keywords k ON pk.keyword_id = k.id
                WHERE k.name like :keyword and p.user_id = :user_id";

        return $this->processSelectStatment($sql, array(":user_id" => $userId, ":keyword" => "%". $keyword . "%"));
    }

    /**
     * @param $userId
     * @param $Id
     * @return array
     */
    public function getPostByUserIdAndId($userId, $Id)
    {
        $sql = "SELECT * FROM ".$this->_dbTable . " WHERE user_id = :user_id AND id = :id";
        return $this->processSelectStatment($sql, array(":user_id" => $userId, ":id" => $Id));
    }

    /**
     * @param $user_id
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function getAllPosts($user_id, $page = 0, $perPage = 20){
        $sql = "SELECT * FROM ".$this->_dbTable . " WHERE user_id = :user_id";
        if($page > 0){
            $offset = $page*$perPage-$perPage;
            $sql .= " LIMIT $perPage OFFSET $offset";
        }
        return $this->processSelectStatment($sql, array(":user_id" => $user_id));
    }

    /**
     * @param $params
     * @return bool
     */
    public function add(array $params) {
        if (!$this->_validator->validateParams($params, $this->_required_fields)) {
            return false;
        }

        if (empty($params['published'])) {
            $params['published'] = 0;
        }
        $blogUtility = new Blog_Utility();
        $slug = $blogUtility->slug($params['title']);

        $user_id = $_SESSION['user_id'];
        $keywords = $params['keywords'];
        $sql = "INSERT INTO ".$this->_dbTable." (title,content,published,user_id,slug,updated_at)
                VALUES (:title, :content, :published, :user_id, :slug, :updated_at)";
        $params = array(':title' => $this->_security->xss_clean($params['title']),
            ':content' => $this->_security->xss_clean($params['content']),
            ':published' => $params['published'],
            ':slug' => $slug,
            ':user_id' => $user_id,
            ':updated_at' => date("Y-m-d"));
        $postId = $this->processQuery($sql, $params);

        $this->_addPostKeyword($postId, $keywords);

        return $postId;
    }

    /**
     * @param $id
     * @param $params
     * @return bool
     */
    public function update($id, array $params) {

        if (!$this->_validator->validateParams($params, $this->_required_fields)) {
            return false;
        }
        if (isset($params['published'])) {
            $params['published'] = 1;
        } else {
            $params['published'] = 0;
        }
        $blogUtility = new Blog_Utility();
        $slug = $blogUtility->slug($params['title']);

        $user_id = $this->current_user();
        $keywords = $params['keywords'];
        $sql = "UPDATE ".$this->_dbTable." SET title = :title, content = :content,
                published = :published,slug = :slug, updated_at = :updated_at WHERE id=:id and user_id = :user_id";
        $params = array(':title' => $this->_security->xss_clean($params['title']),
            ':content' => $this->_security->xss_clean($params['content']),
            ':published' => $params['published'],
            ':slug' => $slug,
            ':updated_at' => date("Y-m-d"),
            ':id' => $id,
            ':user_id' => $user_id);
        $this->processQuery($sql, $params);

        return $this->_updatePostKeywords($id, $keywords);

    }

    /**
     * @param $postId
     * @param $userId
     * @return string
     */
    public function destroy($postId, $userId) {
        $sql = "DELETE FROM posts WHERE id = :postId AND user_id = :userId";
        return $this->processQuery($sql, array(":postId" => $postId, ":userId" => $userId));
    }

    /**
     * @param $postId
     * @return array
     */
    public function getPostKeywords($postId)
    {
        $sql = "SELECT k.id, k.name FROM keywords k LEFT JOIN post_keywords pk ON k.id = pk.keyword_id
        LEFT JOIN posts p ON p.id = pk.post_id WHERE p.id = :id";
        return $this->processSelectStatment($sql, array(":id" => $postId));
    }

    /**
     * @param $keywords array
     * @return mixed
     */
    public function makeCommaSeparated(array $keywords)
    {
        $str = '';
        foreach ($keywords as $keyword) {
           $str .= $keyword['name']. ',';
        }

        return preg_replace("/,\z/", '', $str);
    }

    /**
     * @param $postId
     * @param $keywords
     * @return bool
     */
    protected function _addPostKeyword($postId, $keywords) {
        $keywords = explode(",", $keywords);
        $blogUtility = new Blog_Utility();

        foreach ($keywords as $keyword) {
            if (!empty($keyword)) {
                $slug = $blogUtility->slug($keyword);
                if ($keywordData = $this->_getKeywordBySlug($slug)) {
                    $keywordId = $keywordData[0]['id'];
                } else {
                    $keywordId = $this->_addKeyword($keyword, $slug);
                }

                $sql = "INSERT INTO post_keywords (post_id,keyword_id)
                VALUES (:post_id, :keyword_id)";
                $params = array(':post_id' => $postId,
                    ':keyword_id' => $keywordId);
                $this->processQuery($sql, $params);

            }

        }

        return true;
    }

    /**
     * @param $postId
     * @param $keywords
     * @return bool
     */
    protected function _updatePostKeywords($postId, $keywords)
    {
        $keywords = explode(",", $keywords);
        $blogUtility = new Blog_Utility();
        $this->_removePostkeywords($postId);
        foreach ($keywords as $keyword) {
            if (!empty($keyword)) {
                $slug = $blogUtility->slug($keyword);
                if ($keywordData = $this->_getKeywordBySlug($slug)) {
                    $keywordId = $keywordData[0]['id'];
                } else {
                    $keywordId = $this->_addKeyword($keyword, $slug);
                }
                $sql = "INSERT INTO post_keywords (post_id,keyword_id)
                VALUES (:post_id, :keyword_id)";
                $params = array(':post_id' => $postId,
                    ':keyword_id' => $keywordId);

                $this->processQuery($sql, $params);

            }

        }
        return true;
    }

    /**
     * @param $postId
     * @return string
     */
    protected function _removePostKeywords($postId) {
        $sql = "DELETE FROM post_keywords WHERE post_id = :postId";
        return $this->processQuery($sql, array(":postId" => $postId));
    }

    /**
     * @param $slug
     * @return array
     */
    protected function _getKeywordBySlug($slug) {
        $sql = "SELECT * FROM keywords WHERE slug = :slug";
        return $this->processSelectStatment($sql, array(":slug" => $slug));
    }

    /**
     * @param $keyword
     * @param $slug
     * @return string
     */
    protected function _addKeyword($keyword, $slug) {
        $sql = "INSERT INTO keywords (name,slug)
                VALUES (:name, :slug)";
        $params = array(':name' => $keyword,
            ':slug' => $slug);
        return $this->processQuery($sql, $params);
    }

}