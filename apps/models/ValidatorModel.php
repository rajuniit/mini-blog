<?php

/**
 * This is validator class. use to validate model data.
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 *
 */
class ValidatorModel
{
    /**
     * @var array
     */
    protected $_errors = array();

    /**
     * @param array $params
     * @param array $requiredParams
     * @return bool
     */
    public function validateParams(array $params, array $requiredParams) {
        foreach ($requiredParams as $field) {
            if (!isset($params[$field]) || empty($params[$field])) {
                $this->_errors[] = "The ". $field. " is required.";
            }

            if ($field == 'email') {
                if (!$this->validEmailCheck($params['email'])) {
                   $this->_errors[] = 'The email you entered is invalid';
                   return false;
                }
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function errors()
    {
        return $this->_errors;
    }

    /**
     * @param $value
     * @return int
     */
    public function validEmailCheck($value) {

        return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $value);
    }
}