<?php

/**
 * this class defined some common function use
 * all the models
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class BaseModel extends Bootstrap
{

    /**
     * @var string
     */
    protected $_dbTable = null;

    /**
     * @var string
     */
    protected $_idField = "id";

    /**
     * @var ValidatorModel
     */
    protected $_validator;

    /**
     * @var Blog_Security
     */
    protected $_security;

    /**
     * @var array
     */
    protected $_errors = array();


    /**
     * this function handles all the select statement
     *
     * @param $sql
     * @param array $params
     * @return array
     */
    final public function processSelectStatment($sql, $params = array()) {
        $std = $this->db()->prepare($sql);
        $std->execute($params);
        return $std->fetchAll();

    }

    /**
     * this function handle all the insert,delete,update query
     *
     * @param $sql
     * @param array $params
     * @return string
     */
    final public function processQuery($sql, $params = array()) {
        $std = $this->db()->prepare($sql);
        $std->execute($params);
        return $this->db()->lastInsertId();
    }


    /**
     * @param $id
     * @return string
     */
    public function getById($id){
        $sql = "SELECT * FROM ".$this->_dbTable." WHERE ".$this->_idField." = :id";
        return $this->processSelectStatment($sql, array(":id" => $id));
    }

    /**
     * @param int $page
     * @param int $perPage
     * @return string
     */
    public function getAll($page = 0, $perPage = 20){
        $sql = "SELECT * FROM ".$this->_dbTable;
        if($page > 0){
            $offset = $page*$perPage-$perPage;
            $sql .= " LIMIT $perPage OFFSET $offset";
        }
        return $this->processSelectStatment($sql);
    }

    /**
     * @return array
     */
    public function errors()
    {
        return array_merge($this->_validator->errors(), $this->_errors);
    }

}