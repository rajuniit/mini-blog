<?php
/**
 * this will handle user create.
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class UserModel extends BaseModel
{

    /**
     * @var array
     */
    protected $_required_fields = array('full_name' => 'full_name',
                                        'email' => 'email',
                                        'password' => 'password',
                                        'blog_title' => 'blog_title');

    /**
     * init validator class
     */
    public function __construct() {
        $this->_dbTable = "users";
        $this->_validator = new ValidatorModel();
    }

    /**
     * @param $email
     * @return array
     */
    public function getByEmail($email) {
        $sql = "SELECT * FROM ".$this->_dbTable." WHERE email = :email";
        return $this->processSelectStatment($sql, array(":email" => $email));
    }

    /**
     * @param array $params
     * @return bool|string
     */
    public function add(array $params) {
        if (!$this->_validator->validateParams($params, $this->_required_fields)) {
          return false;
        }
        if ($this->_is_email_exist($params['email'])) {
            return false;
        }
        $sql = "INSERT INTO ".$this->_dbTable." (full_name,email,password,blog_title,updated_at)
                VALUES (:full_name, :email, :password, :blog_title, :updated_at)";
        $params = array(':full_name' => $params['full_name'],
                        ':password' => md5($params['password']),
                        ':email' => $params['email'],
                        ':blog_title' => $params['blog_title'],
                        ':updated_at' => date("Y-m-d"));
        return $this->processQuery($sql, $params);
    }

    /**
     * @param array $params
     * @return array|bool
     */
    public function authenticate(array $params) {
        if (!$this->_validator->validateParams($params, array('email' => 'email', 'password' => 'password'))) {
            return false;
        }

        $user = $this->getByEmail($params['email']);
        if ($user) {
            if ($user[0]['password'] == md5($params['password'])) {
                return $user;
            } else {
                $this->_errors[] = "Sorry Invalid email or password";
                return false;
            }
        } else {
            $this->_errors[] = "Sorry No account found using this email";
            return false;
        }

        return false;
    }

    /**
     * @param array $user
     */
    public function login(array $user)
    {
        $_SESSION['user_id'] = $user[0]['id'];
    }

    /**
     * @param $email
     * @return bool
     */
    protected function _is_email_exist($email)
    {
        $user = $this->getByEmail($email);
        if ($user) {
            $this->_errors[] = "This email already exist. Please try another email";
            return true;
        }

        return false;
    }
}