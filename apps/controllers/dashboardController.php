<?php

/**
 * After logged in user will land here.
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class dashboardController extends Bootstrap
{
    /**
     * check authentication
     */
    public function __construct() {
        parent::__construct();
        $this->checkAuthentication();
    }

    /**
     * listing all blog posts
     */
    public function index()
    {
        $post = new PostModel();
        if (!empty($_GET['q'])) {
            $posts = $post->searchByKeyword($_GET['q'], $this->current_user());
        } else {
            $posts = $post->getAllPosts($this->current_user());
        }

        $this->view()->addViewParam('posts', $posts);

        $this->view()->addView('posts/index.php');
    }
}