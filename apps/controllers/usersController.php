<?php

/**
 * handles user signup,login and logout
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class usersController extends Bootstrap
{
    /**
     * user signup
     */
    public function sign_up()
    {
        if (!empty($_POST)) {
            $user = new UserModel();

            if ($user->add($_POST)) {

                $userData = $user->getByEmail($_POST['email']);
                $user->login($userData);
                $this->redirect("/dashboard/index");

            } else {
                $this->setFlashMessage($user->errors(), 'error');
            }
        }
        $this->view()->addView('users/sign_up.php');

    }

    /**
     * user login
     */
    public function login()
    {
        if ($this->logged_in()) {
            $this->redirect('/dashboard/index');
        }

        if (!empty($_POST)) {
            $user = new UserModel();
            if ($userData = $user->authenticate($_POST)) {
                $user->login($userData);
                $this->redirect("/dashboard/index");
            } else {
                $this->setFlashMessage($user->errors(), 'error');
            }
        }
        $this->view()->addView('users/login.php');
    }

    /**
     * user logout
     */
    public function logout()
    {
        if (!$this->logged_in()) {
            $this->redirect('/users/login');
        }
        $_SESSION['user_id'] = null;
        $this->redirect('/users/login');
    }
}