<?php

/**
 * This class handles to create, update and delete blog
 * posts
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class postsController extends Bootstrap
{
    /**
     * check authentication
     */
    public function __construct() {
        parent::__construct();
        $this->checkAuthentication();
    }

    /**
     * Add Blog Post
     */
    public function add()
    {
        if (!empty($_POST)) {
            $post = new PostModel();
            if ($post->add($_POST)) {
                $this->redirect('/dashboard/index');
            } else {
                $this->setFlashMessage($post->errors(), 'error');
            }
        }
        $this->view()->addView("posts/add.php");
    }

    /**
     * edit blog post
     */
    public function edit()
    {
        $params = func_get_args();
        if (isset($params[2])) {
            $id = intval($params[2]);
        }
        $post = new PostModel();
        $postData = $post->getPostByUserIdAndId($this->current_user(), $id);
        if (!$postData) {
            $this->redirect('/dashboard/index');
        }

        if (!empty($_POST)) {

            if ($post->update($id, $_POST)) {

                $this->redirect('/dashboard/index');

            } else {
                $this->setFlashMessage($post->errors(), 'error');
            }
        }

        $keywords = $post->getPostKeywords($id);
        $commaSeparated = $post->makeCommaSeparated($keywords);
        $this->view()->addViewParam('post', $postData[0]);
        $this->view()->addViewParam('keywords', $commaSeparated);

        $this->view()->addView('posts/edit.php');
    }

    /**
     * delete blog post
     */
    public function destroy()
    {
        $params = func_get_args();
        if (isset($params[2])) {
            $id = intval($params[2]);
        }
        $post = new PostModel();
        $postData = $post->getPostByUserIdAndId($this->current_user(), $id);
        if (!$postData) {
            $this->redirect('/dashboard/index');
        }

        if ($post->destroy($id, $this->current_user())) {
            $this->redirect("/dashboard/index");
        } else {

            $this->setFlashMessage("Something goes wrong. Please try later", 'error');
            $this->redirect("/dashboard/index");
        }

    }
}