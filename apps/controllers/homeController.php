<?php

/**
 * It's a home page.
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class homeController extends Bootstrap
{
    public function index()
    {
        $this->view()->addView('home/index.php');
    }
}