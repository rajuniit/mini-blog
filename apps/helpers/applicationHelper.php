<?php

/**
 * this is helper class and defined some to use from view
 *
 * @package BlogApplication
 * @author Raju Mazumder <rajuniit@gmail.com>
 */
class applicationHelper
{

    /**
     * use this function to check authentication
     *
     * @static
     * @return bool
     */
    public static function is_logged_in() {
        if (isset($_SESSION['user_id'])) {
            return true;
        }

        return false;
    }

    /**
     * use this function to word limit some long text
     *
     * @static
     * @param $str
     * @param int $limit
     * @param string $end_char
     * @return string
     */
    public static function word_limiter($str, $limit = 100, $end_char = '&#8230;') {
        $blogUtility = new Blog_Utility();
        return $blogUtility->word_limiter($str, $limit, $end_char);
    }
}