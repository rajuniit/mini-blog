<form class="form-horizontal" id="registerHere" method='post' action=''>
    <fieldset>
        <legend>Registration</legend>
        <div class="control-group">
            <label class="control-label" for="full_name">Your Full Name</label>

            <div class="controls">
                <input type="text" class="input-xlarge" id="full_name" name="full_name" rel="popover"
                       data-content="Enter your first and last name." data-original-title="Full Name">

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="blog_title">Blog Title</label>

            <div class="controls">
                <input type="text" class="input-xlarge" id="blog_title" name="blog_title" rel="popover"
                       data-content="Enter the blog title." data-original-title="Blog Title">

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="email">Email</label>

            <div class="controls">
                <input type="text" class="input-xlarge" id="email" name="email" rel="popover"
                       data-content="What’s your email address?" data-original-title="Email">

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password">Password</label>

            <div class="controls">
                <input type="password" class="input-xlarge" id="password" name="password" rel="popover"
                       data-content="6 characters or more! Be tricky" data-original-title="Password">

            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="confirm_password">Confirm Password</label>

            <div class="controls">
                <input type="password" class="input-xlarge" id="confirm_password" name="confirm_password" rel="popover"
                       data-content="Re-enter your password for confirmation." data-original-title="Re-Password">

            </div>
        </div>


        <div class="control-group">
            <label class="control-label" for="submit"></label>

            <div class="controls">
                <button type="submit" class="btn btn-success" id="submit" rel="tooltip" title="first tooltip">Create My Account
                </button>

            </div>

        </div>


    </fieldset>
</form>