<form class="form-horizontal" id="loginHere" method='post' action=''>
    <fieldset>
        <legend>Login</legend>

        <div class="control-group">
            <label class="control-label" for="email">Email</label>

            <div class="controls">
                <input type="text" class="input-xlarge" id="email" name="email" rel="popover"
                       data-content="What’s your email address?" data-original-title="Email">

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password">Password</label>

            <div class="controls">
                <input type="password" class="input-xlarge" id="password" name="password" rel="popover"
                       data-content="6 characters or more! Be tricky" data-original-title="Password">

            </div>
        </div>


        <div class="control-group">
            <label class="control-label" for="submit"></label>

            <div class="controls">
                <button type="submit" class="btn btn-success" id="submit" rel="tooltip" title="first tooltip">Login
                </button>

            </div>

        </div>


    </fieldset>
</form>