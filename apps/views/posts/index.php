<div class="page-header">
  <h1>Blog Posts</h1>
</div>

<form class="well form-search" method="get" action="/dashboard/index">
    <input type="text" class="input-medium search-query" name="q" value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''?>">
    <button type="submit" class="btn">Search</button>
</form>

<?php if (count($posts) > 0 && is_array($posts)):?>
<table class="table table-striped">
  <thead>
    <tr>

      <th>Title</th>
      <th>Content</th>
      <th>Published</th>
      <th>Created</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

    <?php foreach ($posts as $post): ?>
      <tr>
        <td><?php echo $post['title'] ?></td>
        <td><?php echo applicationHelper::word_limiter($post['content'], 20) ?></td>
        <td><?php echo $post['published'] == 1 ? 'yes' : 'no' ?></td>
        <td><?php echo $post['created_at']?></td>
        <td>
          <a href="/posts/edit/<?php echo $post['id']?>" class="btn btn-mini">Edit</a>
          <a href="/posts/destroy/<?php echo $post['id']?>" class="btn btn-mini btn-danger">Destroy</a>
        </td>
      </tr>
    <?php endforeach;?>

  </tbody>
</table>
<?php else:?>
<div class="alert alert-info">
    <a class="close" data-dismiss="alert" href="#">×</a>
    No post found.
</div>
<?php endif;?>
<a href="/posts/add" class="btn btn-primary">Add Post</a>
