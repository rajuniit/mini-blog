<form class="form-horizontal" id="posts" method='post' action=''>
    <fieldset>
        <legend>Add Post</legend>
        <div class="control-group">
            <label class="control-label" for="title">Title</label>

            <div class="controls">
                <input type="text" class="input-xlarge" id="title" name="title" rel="popover"
                       data-content="Enter the post title." data-original-title="Title">

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="content">Content</label>

            <div class="controls">
                <textarea class="input-xlarge" id="content" name="content" rel="popover"
                       data-content="Enter the blog content." data-original-title="Content" rows="10" cols="20"></textarea>

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="keywords">Keywords</label>

            <div class="controls">
                <input type="text" class="input-xlarge" id="keywords" name="keywords" rel="popover"
                       data-content="Enter some keywords to categorize your post" data-original-title="Keywords">
                <p class="help-block">comma seperated for multiple</p>

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="published">Published</label>
            <div class="controls">
                <label class="checkbox">
                    <input type="checkbox" id="published" value="1" name="published">
                </label>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="submit"></label>

            <div class="controls">
                <button type="submit" class="btn btn-success" id="submit" rel="tooltip" title="first tooltip">Create Post
                </button>

            </div>

        </div>


    </fieldset>
</form>